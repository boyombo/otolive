from django.contrib import admin

# Register your models here.
from .models import Property, FeaturedProperty, Payment

class NewProjectAdmin(admin.ModelAdmin):
	list_display = ['title', 'updated', 'when']

	class Meta:
		model = Property

class FeaturedPropertyAdmin(admin.ModelAdmin):
	list_display = ['title', 'state', 'address']

	class Meta:
		model = FeaturedProperty

admin.site.register(Property, NewProjectAdmin)

admin.site.register(FeaturedProperty, FeaturedPropertyAdmin)


admin.site.register(Payment)