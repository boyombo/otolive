from django.conf import settings
from django.urls import reverse
from django.db import models
from django.db.models.signals import pre_save
# from filer.fields.imagefields import FilerImageField
from django.utils import timezone
from django.utils.text import slugify
# Create your models here.


def upload_location(instance, filename):
    return "%s/%s" % (instance.id, filename)


class Property(models.Model):
    title = models.CharField(max_length=250)
    state = models.CharField(max_length=250)
    city = models.CharField(max_length=250)
    address = models.CharField(max_length=500)
    slug = models.SlugField(unique=True)
    content = models.TextField()
    image = models.ImageField(upload_to=upload_location, null=True, blank=True)
    image_name = models.CharField(max_length=250, null=True, blank=True)
    height_field = models.IntegerField(default=0)
    width_field = models.IntegerField(default=0)
    draft = models.BooleanField(default=False)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    when = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("update_detail", kwargs={"slug": self.slug})

    class Meta:
        ordering = ["-when", "-updated"]
        verbose_name_plural = 'Properties'


def create_slug(instance, new_slug=None):
    slug = slugify(instance.title)
    if new_slug is not None:
        slug = new_slug
    qs = Property.objects.filter(slug=slug).order_by("-id")
    exists = qs.exists()
    if exists:
        new_slug = "%s-%s" % (slug, qs.first().id)
        return create_slug(instance, new_slug=new_slug)
    return slug


def pre_save_post_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = create_slug(instance)

pre_save.connect(pre_save_post_receiver, sender=Property)


class FeaturedProperty(models.Model):
    title = models.CharField(max_length=500)
    total_price = models.CharField(max_length=250, blank=True, null=True)
    state = models.CharField(max_length=250, blank=True, null=True)
    city = models.CharField(max_length=250, blank=True, null=True)
    address = models.CharField(max_length=500, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    house_image = models.ImageField(
        upload_to=upload_location,
        null=True, blank=True,
        width_field="width_field",
        height_field="height_field")
    floor_image = models.ImageField(
        upload_to=upload_location,
        null=True, blank=True,
        width_field="width_field",
        height_field="height_field")
    height_field = models.IntegerField(default=0)
    width_field = models.IntegerField(default=0)
    when = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("featuredetail", kwargs={"id": self.id})

    class Meta:
        ordering = ["-when", "-updated"]
        verbose_name_plural = 'Featured Properties'


class Payment(models.Model):
    title = models.ForeignKey(FeaturedProperty, on_delete=models.CASCADE)
    months = models.PositiveIntegerField(default=12, null=True)
    percentage = models.DecimalField(
        max_digits=12, decimal_places=2, null=True)
    initial_pay = models.DecimalField(
        max_digits=12, decimal_places=2, null=True)
    installment = models.DecimalField(
        max_digits=12, decimal_places=2, null=True)
    balance = models.DecimalField(max_digits=12, decimal_places=2, null=True)
    total_price = models.DecimalField(
        max_digits=12, decimal_places=2, null=True)
    # months = models.CharField(max_length=50, blank=True, null=True)
    # percentage = models.CharField(max_length=250, blank=True, null=True)
    # initial_pay = models.CharField(max_length=250, blank=True, null=True)
    # installment = models.CharField(max_length=250, blank=True, null=True)
    # balance = models.CharField(max_length=250, blank=True, null=True)
    # total_price = models.CharField(max_length=250, blank=True, null=True)

    def __str__(self):
        return '{} months {}'.format(self.months, self.title.title)
