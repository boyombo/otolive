from django.db import models

from modelcluster.fields import ParentalKey

from wagtail.core.models import Page, Orderable
from wagtail.core.fields import RichTextField
from wagtail.admin.edit_handlers import FieldPanel, InlinePanel
from wagtail.snippets.edit_handlers import SnippetChooserPanel
from wagtail.images.edit_handlers import ImageChooserPanel

from gallery.models import Estate


class ListingIndexPage(Page):
    intro = RichTextField(blank=True)

    def get_context(self, request):
        context = super().get_context(request)
        listings = self.get_children().live().order_by('-first_published_at')
        context['listings'] = listings
        return context

    content_panels = Page.content_panels + [
        FieldPanel('intro', classname='full'),
    ]

    subpage_types = ['listing.ListingPage']


class ListingPage(Page):
    body = RichTextField()
    #cost = models.CharField(max_length=20)
    cost = models.DecimalField(max_digits=15, decimal_places=2)
    address = models.TextField(blank=True)
    estate = models.ForeignKey(Estate, null=True, on_delete=models.SET_NULL)
    model_pic = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+')
    plan_pic = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+')

    content_panels = Page.content_panels + [
        FieldPanel('body', classname='full'),
        FieldPanel('cost'),
        FieldPanel('address', classname='full'),
        SnippetChooserPanel('estate'),
        ImageChooserPanel('model_pic'),
        ImageChooserPanel('plan_pic'),
        InlinePanel('listing_payments', label='Payment Plan'),
        InlinePanel('listing_rooms', label='Rooms'),
        InlinePanel('listing_images', label='Plan Images'),
        #ImageChooserPanel('top_left_pic'),
        #ImageChooserPanel('top_right_pic'),
        #ImageChooserPanel('bottom_left_pic'),
        #ImageChooserPanel('bottom_right_pic'),
    ]


class PlanImages(Orderable):
    page = ParentalKey(ListingPage, related_name='listing_images')
    image = models.ForeignKey(
        'wagtailimages.Image',
        on_delete=models.CASCADE,
        related_name='+'
    )
    caption = models.CharField(max_length=100, blank=True)

    panels = [
        ImageChooserPanel('image'),
        FieldPanel('caption'),
    ]


class ListingPayment(Orderable):
    page = ParentalKey(ListingPage, related_name='listing_payments')
    month = models.CharField(max_length=10)
    percent = models.DecimalField(max_digits=5, decimal_places=2)
    balance = models.DecimalField(max_digits=15, decimal_places=2)

    panels = [
        FieldPanel('month'),
        FieldPanel('percent'),
        FieldPanel('balance'),
    ]

    @property
    def initial_payment(self):
        return self.percent * self.page.cost / 100

    @property
    def total_amount(self):
        return self.initial_payment + self.balance


class BuildingRoom(Orderable):
    page = ParentalKey(ListingPage, related_name='listing_rooms')
    title = models.CharField(max_length=50)
    model_pic = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+')

    panels = [
        FieldPanel('title'),
        ImageChooserPanel('model_pic')
    ]

    def __str__(self):
        return self.title
