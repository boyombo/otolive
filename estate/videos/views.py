from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from django.shortcuts import render, HttpResponse, HttpResponseRedirect, Http404
from django.utils.safestring import mark_safe
# Create your views here.
from .models import Video


def video_list(request):
	videos_list = Video.objects.all()
	query = request.GET.get("q")
	if query:
		videos_list = videos_list.filter(
			Q(title__icontains=query) |
			Q(embed_code__icontains=query) |
			Q(description__icontains=query)			
			).distinct()
	paginator = Paginator(videos_list, 16)
	page_request_var = "page"
	page = request.GET.get(page_request_var)
	try:
		queryset = paginator.page(page)
	except PageNotAnInteger:
		#serve first page if page is not an integer
		queryset = paginator.page(1)
	except EmptyPage:
		queryset = paginator.page(paginator.num_pages)
	return render(request, 'videos/video_list.html', {"videos": queryset, "page_request_var": page_request_var})


def video_detail(request, id):
	try:
		video = Video.objects.get(id=id)
		return render(request, 'videos/video_details.html', {"video": video})
	except:
		raise Http404