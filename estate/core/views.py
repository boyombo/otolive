from django.shortcuts import render, get_object_or_404
from projects.models import FeaturedProperty


def home(request):
    projects = FeaturedProperty.objects.all()[:20]
    return render(request, 'home.html', {"featured_projects": projects})


def about(request):
    return render(request, 'about.html', {})


def featuredetail(request, id=None):
    instance = get_object_or_404(FeaturedProperty, id=id)
    price_plan = instance.payment_set.all()
    projects = FeaturedProperty.objects.all()[:3]
    return render(
        request,
        "projects/featured_detail.html",
        {
            "instance": instance,
            "price_plan": price_plan,
            "projects": projects
        })
